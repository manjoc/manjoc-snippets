# manjoc-snippets

## Typescript snippets

`comp`: generate a basic typescript component (with imports, props interface, export...)  
`compn`: same as but _comp_ + auto-set the component name with the file name

## Styleguide snippets

`doc`: generate a doc component for styleguide
